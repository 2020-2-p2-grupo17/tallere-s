#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]){
	
	umask(0);

	char *rutafuente= argv[1];
	char *rutadestino= argv[2];
	
	//leer los bytes de un archivo
	int fd = open(rutafuente,O_RDONLY);

	if(fd == -1){
		perror("Ha ocurrido un error al abrir el archivo");
	}
	
	//escribirlos a otro archivo 
	int fd2 = open(rutadestino,O_CREAT | O_TRUNC|O_WRONLY, 0666);

	if(fd2 == -1){
		perror("Ha ocurrido un error al abrir el archivo");
	}
	
	char buf[1000];
	int total = 0;
	int linea;
	while( (linea = read(fd,buf,1000)) !=0){
		int suma = write(fd2,buf,linea);
		total = total+suma;
	}
	printf("%i bytes copiados.\n",total);

	close(fd);
	close(fd2);

	return 0;
}


